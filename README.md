# Projet Chef D'Oeuvre

L'objectif de ce projet sera de réaliser une application client serveur sur le thème de votre choix.
Vous imaginerez deux ou trois idées d'applications différentes et nous en choisirons que vous réaliserez.

Une fois l'application choisie, voici les contraintes :


### Fonctionnalités minimales 
* Authentification JWT
* Au moins 3 entités
### Conception
* Réalisation de diagrammes de Use Case
* Réalisation d'un diagramme de classe
* Réalisation de maquettes (wireframe) mobile first
### Technique
* Utilisation Symfony 4.2 pour faire le back en API Rest
* Utilisation de Vue.js pour faire le front
* Faire des tests fonctionnels côté back et tester unitairement vos components côté front
* Application responsive (bootstrap)
* Utilisation de git/gitlab pour l'organisation du projet
### Organisation Agile
* Faire des Users Stories décrivant vos fonctionnalités
* 4 sprints (un par semaine) avec les fonctionnalités à livrer
* Utiliser la partie Issues et les Boards du projet gitlab pour définir vos tâches et votre kanban

Vous réaliserez également un README avec vos différents diagrammes commentés et une petite présentation du projet.

